import { NgModule } from '@angular/core';
import {
  MatButtonModule, MatToolbarModule, MatIconModule, MatCardModule,
  MatFormFieldModule, MatInputModule, MatSidenavModule, MatNavList, MatListModule, MatRippleModule
} from '@angular/material';


@NgModule({
  imports:	[MatRippleModule, MatListModule, MatSidenavModule, MatButtonModule,	MatToolbarModule,	MatIconModule, 	MatCardModule, MatFormFieldModule, MatInputModule],
  exports:	[MatRippleModule, MatListModule, MatSidenavModule, MatButtonModule,	MatToolbarModule,	MatIconModule, 	MatCardModule, MatFormFieldModule, MatInputModule],
  declarations: []
})
export class MaterialModule { }
