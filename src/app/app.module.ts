import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { SideNavComponent } from './side-nav/side-nav.component';
import	{	FormsModule,	ReactiveFormsModule	}	from	'@angular/forms';
import { CitySearchComponent } from './city-search/city-search.component';
// import {
//   MatButtonModule, MatToolbarModule, MatIconModule, MatCardModule,
//   MatFormFieldModule,
//   MatInputModule
// } from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    CitySearchComponent,
    SideNavComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule
  //  MatButtonModule,	MatToolbarModule,	MatIconModule, 	MatCardModule, MatFormFieldModule, MatInputModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
